<?php

namespace ContextualCode\EzPlatformCustomAttributes\UI\Config\Provider\FieldType\RichText;

use EzSystems\EzPlatformAdminUi\UI\Config\ProviderInterface;

/**
 * Provide information about RichText Custom Classes.
 */
class CustomClass implements ProviderInterface
{
    /**
     * @var array
     */
    private $customClasses;

    /**
     * @param array $customClasses
     */
    public function __construct(array $customClasses)
    {
        $this->customClasses = $customClasses;
    }

    /**
     * @return array RichText Custom Classes config
     */
    public function getConfig(): array
    {
        return $this->customClasses;
    }
}
