<?php

namespace ContextualCode\EzPlatformCustomAttributesBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\Yaml\Yaml;

class EzPlatformCustomAttributesExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAlias(): string
    {
        return 'ezrichtext_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $configuation = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuation, $configs);

        $customClasses = $config['custom_classes'] ?? [];
        $customAttributes = $config['custom_attributes'] ?? [];
        $embedViews = $config['embed_views'] ?? [];

        $container->setParameter('ezrichtext.custom_classes', $customClasses);
        $container->setParameter('ezrichtext.custom_attributes', $customAttributes);
        $container->setParameter('ezrichtext.embed_views', $embedViews);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguration(array $configs, ContainerBuilder $container): Configuration
    {
        return new Configuration();
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container): void
    {
        $this->prependEzRichTextConfiguration($container);
        $this->prependBazingaJsTranslationConfiguration($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function prependEzRichTextConfiguration(ContainerBuilder $container): void
    {
        $configFile = __DIR__ . '/../Resources/config/ezrichtext.yml';
        $config = Yaml::parseFile($configFile);
        $container->prependExtensionConfig('ezrichtext', $config);
        $container->addResource(new FileResource($configFile));
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function prependBazingaJsTranslationConfiguration(ContainerBuilder $container): void
    {
        $configFile = __DIR__ . '/../Resources/config/bazinga_js_translation.yml';
        $config = Yaml::parseFile($configFile);
        $container->prependExtensionConfig('bazinga_js_translation', $config);
        $container->addResource(new FileResource($configFile));
    }
}