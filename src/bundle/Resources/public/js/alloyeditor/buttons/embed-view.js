import React from 'react';
import AlloyEditor from 'alloyeditor';
import DropDownList from '../components/drop-down-list';

import EzWidgetButton from './../../../../../../../../../ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/alloyeditor/src/base/ez-widgetbutton';

class BtnEmbedView extends EzWidgetButton {
    static get key() {
        return 'embed-view';
    }

    /**
     * Lifecycle. Invoked once, both on the client and server, immediately before the initial rendering occurs.
     *
     * @method componentWillMount
     */
    componentWillMount() {
        const view = this.getActiveView();
        const name = this.getViewTitle(view);
        this.views = this.getViews({id: view, title: name});
    }

    /**
     * Returns active embed view
     *
     * @method getActiveView
     */
    getActiveView() {
        return this.getWidget().element.data('ezview');
    }

    /**
     * Applies view attribute
     *
     * @method applyView
     */
    applyView(view) {
        const widget = this.getWidget();

        widget.setData('view', view.id);
        widget.element.setAttribute('data-ezview', view.id);
        widget.focus();
    }

    /**
     * Lifecycle. Renders the UI of the button.
     *
     * @method render
     * @return {Object} The content which should be rendered.
     */
    render() {
        const view = this.getActiveView();
        const name = this.getViewTitle(view);
        const title = Translator.trans('embed.view.title', {view: name}, 'custom_attributes');

        if (this.views.length <= 1) {
            return null;
        }

        let embedViewsList;
        let openDropDownArrow = (<span className="ae-icon-arrow"/>);
        if (this.props.expanded) {
            embedViewsList = (
                <DropDownList
                    callback={this.applyView.bind(this)}
                    items={this.views}
                    activeItem={view}
                />
            );
        }

        return (
            <div className="ae-container-dropdown ae-has-dropdown">
                <button
                    aria-expanded={this.props.expanded}
                    aria-label={title}
                    className="ae-toolbar-element"
                    onClick={this.props.toggleDropdown}
                    role="combobox"
                    tabIndex={this.props.tabIndex}
                    title={title}
                >
                    <div className="ae-container">
                        <span className="ae-container-dropdown-selected-item">
                            {name}
                        </span>
                        {openDropDownArrow}
                    </div>
                </button>
                {embedViewsList}
            </div>
        );
    }

    getViewTitle(id) {
        const key = 'embed.view.' + id;
        const translation = Translator.trans(key, {}, 'custom_attributes');

        return key === translation ? id : translation;
    }

    getViews(current) {
        // Append views from config
        let views = eZ.adminUiConfig.richTextEmbedViews.map(id => {
            return {
                id,
                title: this.getViewTitle(id)
            }
        });
        // Check if current view is in the views list
        let isCurrentInTheList = false;
        views.forEach(function (view) {
            if (current.id === view.id) {
                isCurrentInTheList = true;
            }
        });
        // Add current view to the list if it is missing
        if (isCurrentInTheList === false) {
            views = [current].concat(views);
        }

        return  views;
    }
}

AlloyEditor.Buttons[BtnEmbedView.key] = AlloyEditor.BtnEmbedView = BtnEmbedView;
eZ.addConfig('ezAlloyEditor.BtnEmbedView', BtnEmbedView);