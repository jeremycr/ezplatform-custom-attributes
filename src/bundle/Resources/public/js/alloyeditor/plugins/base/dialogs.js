const ShowCustomAttributesDialogs = {
    getDefinitions: function(editor, styles, attributes) {
        const config = this.mapConfigByTag(styles, attributes);

        let definitions = {};
        for (let [tag, settings] of Object.entries(config)) {
            let definition = this.getDefaultDefinition(tag);

            if (Object.entries(settings.styles).length > 0) {
                definition.contents[0].elements.push(this.getStyleElement(tag, settings.styles));
            }

            if (Object.entries(settings.attributes).length > 0) {
                for (let [attribute, config] of Object.entries(settings.attributes)) {
                    definition.contents[0].elements.push(
                        this.getAttributeElement(tag, attribute, config)
                    );
                }
            }

            definitions[tag] = definition;
        }

        return definitions;
    },
    mapConfigByTag: function(styles, attributes) {
        let tags = [];
        [styles, attributes].forEach(function(config) {
            for (let [tag, value] of Object.entries(config)) {
                if (tags.indexOf(tag) === -1) {
                    tags.push(tag);
                }
            }
        });

        let mappedConfigs = {};
        tags.forEach(function(tag) {
            mappedConfigs[tag] = {
                styles: styles.hasOwnProperty(tag) ? styles[tag] : [],
                attributes: attributes.hasOwnProperty(tag) ? attributes[tag] : [],
            };
        });

        return mappedConfigs;
    },
    getDefaultDefinition: function(tag) {
        const title = Translator.trans('header', {tag: this.getElementLabel(tag)}, 'custom_attributes');
        return {
            title: title,
            minWidth: 250,
            minHeight: 40,
            buttons: [CKEDITOR.dialog.okButton],
            contents: [{
                id: 'custom-attributes',
                elements: []
            }],
            onShow: function () {
                this.setupContent();
            },
            onOk: function() {
                this.commitContent();
            }
        }
    },
    getStyleElement: function(tag, styles) {
        let items = [[Translator.trans('options.empty.label', {}, 'custom_attributes'), '']];
        styles.forEach(function(style) {
            items.push([this.getOptionLabel(style), style]);
        }, this);

        return {
            id: tag + '-custom-style',
            type: 'select',
            multiple: true,
            size: 5,
            inputStyle: 'height: auto',
            label: Translator.trans('custom_style.label', {}, 'custom_attributes'),
            title: Translator.trans('custom_style.title', {}, 'custom_attributes'),
            items: items,
            setup: function () {
                let className = String(this.getDialog().getParentEditor().elementsPathActiveNode.getAttribute('class'))
                    .replace('is-block-focused', '')
                    .trim();
                className = ' ' + className + ' ';

                const options = this._.select.getElement().$.childNodes;
                for (let i = 0; i < options.length; i++) {
                    options[i].selected = className.indexOf(' ' + options[i].value + ' ') !== -1;
                }
            },
            onHide: function() {
                const options = this._.select.getElement().$.selectedOptions;
                const newClasses = [];
                for (let i = 0; i < options.length; i++) {
                    if (options[i].value) {
                        newClasses.push(...options[i].value.split(' '));
                    }
                }

                const element = this.getDialog().getParentEditor().elementsPathActiveNode;
                element.$.className = '';
                if (newClasses.length) {
                    element.$.classList.add(...newClasses);
                }
            }
        };
    },
    getAttributeElement: function(tag, attr, config) {
        const dataProperty = 'data-ezattribute-' + attr;
        let attribute = {
            id: tag + '-custom-attribute-' + attr,
            type: config.hasOwnProperty('type') ? config.type : 'text',
            label: this.getAttributeLabel(attr),
            title: this.getAttributeTitle(attr),
            setup: function () {
                let value = this.getDialog().getParentEditor().elementsPathActiveNode.getAttribute(dataProperty);
                if (config.type === 'checkbox') {
                    value = value ? value.toLowerCase() === 'true' : false;
                }

                if (config.type === 'select' && config.options.indexOf(value) === -1) {
                    return;
                }

                this.setValue(value);
            },
            commit: function() {
                this.getDialog().getParentEditor().elementsPathActiveNode.setAttribute(dataProperty, this.getValue());
            }
        };

        if (config.hasOwnProperty('default') && config.default) {
            attribute.default = config.default;
        }

        if (config.type === 'select') {
            attribute.items = [[Translator.trans('options.empty.label', {}, 'custom_attributes'), '']];
            if (config.options.length > 0) {
                config.options.forEach(function(item) {
                    attribute.items.push([this.getOptionLabel(item), item]);
                }, this);
            }
        }

        return attribute;
    },
    getOptionLabel: function(id) {
        return this.getTranslation('options', id, 'label', id);
    },
    getAttributeLabel: function(id) {
        return this.getTranslation('attributes', id, 'label', id);
    },
    getElementLabel: function(tag) {
        return this.getTranslation('elements', tag, 'label', tag);
    },
    getAttributeTitle: function(id) {
        return this.getTranslation('attributes', id, 'title', '');
    },
    getTranslation: function(section, id, option, defaultValue) {
        const key = section+ '.' + id + '.' + option;
        const translation = Translator.trans(key, {}, 'custom_attributes');

        return key === translation ? defaultValue : translation;
    }
};

export default ShowCustomAttributesDialogs;